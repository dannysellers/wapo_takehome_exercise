import * as React from 'react';
import { Link } from 'react-router-dom';

import { Container } from "./common";

const MovieListItem = ({ movie }) => {
  const { movieTitle, movieYear, id } = movie;
  return <Link to={`/movie/${id}`}>
    {movieTitle} ({movieYear})
  </Link>;
}

const MovieList = () => {
  const [movieData, setMovieData] = React.useState([]);

  React.useEffect(() => {
    fetch('//127.0.0.1:5000/movie/list')
      .then((res) => res.json())
      .then(setMovieData)
      .catch(console.error);
  }, []);

  if (!movieData.length) {
    return <Container>Loading...</Container>;
  }

  return <Container>
    <ul style={{ paddingLeft: '0.5rem' }}>
      {movieData.map((movie, idx) => <li key={idx} style={{ marginBottom: '1rem' }}>
        <MovieListItem movie={movie} />
      </li>)}
    </ul>
  </Container>
}

export default MovieList;
