import * as React from 'react';

import './MovieCard.css';

/*
type MovieListType = {
  id: string,
  movieTitle: string,
  movieYear: string,
  movieLength: string,
  moviePlot: string,
  moviePoster: string,
  contentType: string
}
 */

const Title = ({
  movie: { movieTitle, movieYear }
}) => <div className="title">
  <h2>{movieTitle}</h2>&nbsp;({movieYear})
</div>;

const MovieCard = ({ movie }) => {
  const { movieTitle, movieYear, movieLength, moviePlot, moviePoster } = movie;

  return <div className="container">
    <Title movie={movie} />
    <div className="poster">
      <img src={moviePoster} alt={`${movieTitle} (${movieYear})`}/>
    </div>
    <div className="info">
      <p>{moviePlot}</p>
      <p>{movieLength}</p>
    </div>
  </div>;
}

export default MovieCard;
