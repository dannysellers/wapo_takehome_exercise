import React, { useEffect, useState } from "react";
import {
  useParams
} from "react-router-dom";

import MovieCard from "./MovieCard";
import { Container } from "./common";

const MovieDetail = () => {
  const { id } = useParams();
  const [movieData, setMovieData] = useState({});

  useEffect(() => {
    fetch(`http://127.0.0.1:5000/movie/detail/${id}`)
      .then(res => res.json())
      .then(data => {
        setMovieData(data);
      })
      .catch(err => console.error(err));
  }, [id]);

  const {
    movieTitle,
    Error,
  } = movieData;

  if (Error) return <Container>Error: {Error}</Container>;
  if (!movieTitle) return <Container>Loading...</Container>;

  return <Container>
    <MovieCard movie={movieData} />
  </Container>
}

export default MovieDetail;
