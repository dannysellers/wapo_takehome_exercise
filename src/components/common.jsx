import * as React from 'react';

export const Container = ({ children }) => <div style={{ marginLeft: '2rem' }}>
  {children}
</div>;
