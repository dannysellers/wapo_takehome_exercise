import flask
from flask import jsonify
from flask_cors import CORS, cross_origin
import requests
import pdb

app = flask.Flask(__name__)
app.config["DEBUG"] = True
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

MOVIES = [{
    "id": "tt0110357",
    "movieTitle": "The Lion King",
    "movieYear": "1994",
    "movieLength": "88 min",
    "moviePlot": "Lion prince Simba and his father are targeted by his bitter uncle, who wants to ascend the throne himself.",
    "moviePoster": "https://m.media-amazon.com/images/M/MV5BYTYxNGMyZTYtMjE3MS00MzNjLWFjNmYtMDk3N2FmM2JiM2M1XkEyXkFqcGdeQXVyNjY5NDU4NzI@._V1_SX300.jpg",
    "contentType": "movie"
}, {
    "id": "tt3896198",
    "movieTitle": "Guardians of the Galaxy Vol. 2",
    "movieYear": "2017",
    "movieLength": "136 min",
    "moviePlot": "The Guardians struggle to keep together as a team while dealing with their personal family issues, notably Star-Lord's encounter with his father the ambitious celestial being Ego.",
    "moviePoster": "https://m.media-amazon.com/images/M/MV5BNjM0NTc0NzItM2FlYS00YzEwLWE0YmUtNTA2ZWIzODc2OTgxXkEyXkFqcGdeQXVyNTgwNzIyNzg@._V1_SX300.jpg",
    "contentType": "movie"
}]

# http://www.omdbapi.com/?i=tt3896198&apikey=98fd125
OMDB_API_KEY = '98fd125'


@app.route('/movie/list', methods=['GET'])
@cross_origin()
def movie_list():
    return jsonify(MOVIES)


@app.route('/movie/detail/<string:movie_id>', methods=['GET'])
@cross_origin()
def movie_detail(movie_id):
    # http://www.omdbapi.com/?i=tt0110357&apikey=98fd125
    req = requests.get(f'http://www.omdbapi.com?apikey={OMDB_API_KEY}&i={movie_id}')
    resp = req.json()

    if req.status_code != 200 or resp['Response'] == 'False':
        # OMDBAPI may return 200 with error object; change to error status code if needed
        return jsonify(resp), 400 if req.status_code == 200 else req.status_code

    transformed_response = {
        f'movie{k}': resp[k] for k in ['Title', 'Year', 'Plot', 'Poster']
    }
    transformed_response.update({
        'contentType': resp['Type'],
        'id': resp['imdbID'],
        'movieLength': resp['Runtime']
    })

    return jsonify(transformed_response), req.status_code


app.run()
